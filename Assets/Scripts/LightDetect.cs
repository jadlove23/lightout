﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightDetect : MonoBehaviour
{
  [SerializeField]
  private bool inTheShadow = false;
  [SerializeField]
  private LayerMask LayerMask;
  private void OnTriggerStay2D(Collider2D collision)
  {
    if (collision.CompareTag("Light"))
    {
      float distance = Vector3.Distance(transform.position, collision.gameObject.transform.position);
      Vector3 rayDirection = (collision.gameObject.transform.position - transform.position).normalized;
      RaycastHit2D hit = Physics2D.Raycast(transform.position, rayDirection, distance, LayerMask);
      if (hit.collider != null)
      {
        inTheShadow = true;
      }
      else
      {
        Debug.DrawRay(transform.position, rayDirection * distance, Color.red);
        inTheShadow = false;
      }
    }
  }

  private void OnTriggerEnter2D(Collider2D collision)
  {
    if (collision.CompareTag("Light"))
    {
      float distance = Vector3.Distance(transform.position, collision.gameObject.transform.position);
      Vector3 rayDirection = (collision.gameObject.transform.position - transform.position).normalized;
      RaycastHit2D hit = Physics2D.Raycast(transform.position, rayDirection, distance, LayerMask);
      if (hit.collider != null)
        inTheShadow = true;
      else
        inTheShadow = false;
    }
  }

  private void OnTriggerExit2D(Collider2D collision)
  {
    if (collision.CompareTag("Light"))
    {
      inTheShadow = true;
    }
  }
}
