﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
  private enum MovementState { idle, running, jumping, falling }
  private Rigidbody2D _Rigibody;
  private BoxCollider2D _Collider;
  private SpriteRenderer _Sprite;
  private Animator _Animation;
  private float _DirectionX = 0f;

  [Header("Movement")]
  [SerializeField] private LayerMask _JumpableGroundLayer;
  [SerializeField] private float _MoveSpeed = 7f;
  [SerializeField] private float _JumpForce = 14f;
  private bool _IsGrounded => Physics2D.BoxCast(_Collider.bounds.center, _Collider.bounds.size, 0f, Vector2.down, 0.1f, _JumpableGroundLayer);

  [Header("WallSliding")]
  [SerializeField] private float _CheckRadius;
  [SerializeField] private Transform _WallCheck;
  [SerializeField] private float _WallSlidingSpeed;
  private bool _WallSliding => _IsTouchingWall && !_IsGrounded && _DirectionX != 0;


  [Header("WallJumping")]
  [SerializeField] private float _XWallForce;
  [SerializeField] private float _YWallForce;
  [SerializeField] private float _WallJumptime;
  private bool _WallJumping;
  private bool _IsTouchingWall => Physics2D.OverlapCircle(_WallCheck.position, _CheckRadius, _JumpableGroundLayer);



  private void Start()
  {
    _Rigibody = GetComponent<Rigidbody2D>();
    _Collider = GetComponent<BoxCollider2D>();
    _Sprite = GetComponent<SpriteRenderer>();
    _Animation = GetComponent<Animator>();
  }

  private void Update()
  {
    Movement();
  }

  private void FixedUpdate()
  {
    UpdateAnimationState();
  }

  private void Movement()
  {
    _DirectionX = Input.GetAxisRaw("Horizontal");
    _Rigibody.velocity = new Vector2(_DirectionX * _MoveSpeed, _Rigibody.velocity.y);
    if (Input.GetButtonDown("Jump") && _IsGrounded)
    {
      _Rigibody.velocity = new Vector2(_Rigibody.velocity.x, _JumpForce);
    }
    WallSlide();
    WallJump();
  }

  private void WallSlide()
  {
    if (_WallSliding)
    {
      _Rigibody.velocity = new Vector2(_Rigibody.velocity.x, -_WallSlidingSpeed);
    }

  }

  private void WallJump()
  {
    if (Input.GetButtonDown("Jump") && _WallSliding)
    {
      _WallJumping = true;
      Invoke("SetWallJumpingToFalse", _WallJumptime);
    }
    if (_WallJumping == true)
    {
      _Rigibody.velocity = new Vector2(_XWallForce * -_DirectionX, _YWallForce);
    }

  }

  private void SetWallJumpingToFalse()
  {
    _WallJumping = false;
  }

  private void UpdateAnimationState()
  {
    MovementState state;

    if (_DirectionX > 0f)
    {
      state = MovementState.running;
      _Sprite.flipX = false;
    }
    else if (_DirectionX < 0f)
    {
      state = MovementState.running;
      _Sprite.flipX = true;
    }
    else
    {
      state = MovementState.idle;
    }

    if (_Rigibody.velocity.y > .1f)
    {
      state = MovementState.jumping;
    }
    else if (_Rigibody.velocity.y < -.1f)
    {
      state = MovementState.falling;
    }

    _Animation.SetInteger("state", (int)state);
  }
}